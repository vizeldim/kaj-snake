
import Page from './template/pageTemplates.js';
import { createBasePageElem,createCardElem,createCardListElem,createClipartElem,createToggleSwitch } from '../elements/htmlContentFactory.js';

export class SettingsPage extends Page {
    constructor(id, services) {
        super(id, services)
        
        this._onlineService = services.onlineService
        this._weatherService = services.weatherService
        this._locationService = services.locationService

        this._service = services.settingsService
        this._settings = {}

        this._service
            .getSettings()
            .then(s => {
                this._settings = s
            
                const {content, wrapper} = createBasePageElem('snake game')
                this._elem = content
        
                const clipart = createClipartElem('./img/snake-clipart.png')
                wrapper.append(this.settingsCard,  clipart)    
            })       
            
    }

    _registerOnlineEvents(elem) {
        const status = document.createElement('li')
        const wElem = document.createElement('li')
        const lElem = document.createElement('li')

        const setStatus = (online) => {
            let o = online
            if(!(o === false || o === true)) {
                o = this._onlineService.isOnline
            }

            
            const t = (o === true) ? 'online' : 'offline'
            status.textContent = t
            status.classList.add((o === true) ? 'online' : 'offline')
            status.classList.remove((o === true) ? 'offline' : 'online')


            elem.innerHTML = ''
            elem.append(status, wElem, lElem)

            wElem.textContent = 'Loading...'
            lElem.textContent = 'Loading...'
            this._locationService
                .getCurrentCoords()
                .then(({lat, long}) => {
                    wElem.textContent = `Coords [${lat}, ${long}]`
                    
                    this._weatherService.coords = {lat, long}
                    this._weatherService
                        .getCurrentWeather()
                            .then(w => {
                                lElem.textContent = `${w.location} ${w.temp} °C [${(w.precip >= 0.2) ? "Raining" : "Not Raining"}]`
                            })
                            .catch(e => {lElem.textContent = 'Could not get weather'})
                })
                .catch(e => {
                    lElem.textContent = ''
                    wElem.textContent = 'Could not get location'})

           
        }

        setStatus(this._onlineService.isOnline)

        this._onlineService.addOnlineEvent(() => {
            setStatus(true)
            
        })

        this._onlineService.addOfflineEvent(() => {
            setStatus(false)
        })

        this._locationService.registerPosChangeEvents(
            ()=>{setStatus()},
            ()=>{setStatus()})
    }


    get settingsCard() {
        const innerContent = document.createElement('section')
        innerContent.classList.add('inner-content')
      
        const [card, back] = createCardElem('settings', this.settingsElem)
        innerContent.appendChild(card)

        back.addEventListener('click', (e) => {
            e.preventDefault()
            this._router.navigate('main')
        })

        const onlineData = document.createElement('ul')
        onlineData.classList.add('status-info')


        this._registerOnlineEvents(onlineData)

        innerContent.appendChild(onlineData)

        return innerContent
    }

    get settingsElem() {
        const isColorByWeather = this._settings.colorByWeather
        const weatherToggler = createToggleSwitch(isColorByWeather, (active) => {
            this._settings.colorByWeather = active
            this._service.saveSettings({...this._settings})
        })

        const isSoundActive = this._settings.isSoundActive
        const soundToggler = createToggleSwitch(isSoundActive, (active) => {
            this._settings.isSoundActive = active
            this._service.saveSettings({...this._settings})
        })
        

        const menuListItems = [
            {term: 'Sound', def: soundToggler},
            {term: 'Snake color by weather', def: weatherToggler}
        ]

        return createCardListElem(menuListItems)
    }

    get htmlContent () {
        return this._elem
    }
}