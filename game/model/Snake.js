import { Dir, getDir } from "../utils/directions.js"

export class Snake {
    constructor({x, y, tileLen, boardW, boardH}) {
        this.tileLen = tileLen
        this.boardSize = {w: boardW, h: boardH}

        this.head = {x: x + 10, y}
        this.headShow = 0

        this.tail = {x, y}
        this.tailHide = 0

        this.state = [
            this.tail,
            {x: x, y: y + 5},
            {x: x + 10, y: y + 5},
            {x: x + 10, y: y},
            this.head
        ]
        
        this.len = this._calcLen()
        this.dir = Dir.RIGHT

        this.growing = 0
    }

    // Calculate full snake length
    _calcLen() {
        let len = 1
        for (let i = 0; i < this.state.length - 1; ++i) {
            const start = this.state[i].x + this.state[i].y;
            const end = this.state[i + 1].x + this.state[i + 1].y;
            let l = end - start
            l = (l < 0) ? -l : l
            len += l
        }
        return len
    }

    // Eat food
    eat() {
        this.growing += 1 * this.tileLen
        ++this.len
    }

    // move by 'change' in given 'dir'
    move(dir, change) {
        if(this.tailHide >= this.tileLen) {
            this.tailHide %= this.tileLen 

            if(this.growing <= 0) {
                const tailDir = getDir(this.tail, this.state[1]) 
                if(tailDir === Dir.RIGHT) { ++(this.tail.x) }
                if(tailDir === Dir.LEFT) { --(this.tail.x) }
                if(tailDir === Dir.UP) { --(this.tail.y) }
                if(tailDir === Dir.DOWN) { ++(this.tail.y) }
                
                if( this.tail.x === this.state[1].x &&
                    this.tail.y === this.state[1].y ) {
                    this.state.shift()
                    this.tail = this.state[0]
                    if((this.tail.split === true)){
                        this.state.shift()
                        this.tail = this.state[0]
                    }
                }
            }
        
        }

        if(this.headShow >= this.tileLen) {
            this.headShow %= this.tileLen 

            if(this.dir === Dir.RIGHT) {
                ++(this.head.x) 
                if(this.head.x >= this.boardSize.w) {
                    this.state[this.state.length - 1]['split'] = true
                    this.head = {...this.head, x: 0, split: false}
                    this.state.push(this.head)
                    this.head = {...this.head}
                    this.state.push(this.head)
                }
            }
            if(this.dir === Dir.LEFT) { 
                --(this.head.x)
                if(this.head.x < 0) {
                    this.state[this.state.length - 1]['split'] = true
                    this.head = {...this.head, x: this.boardSize.w - 1, split: false}
                    this.state.push(this.head)
                    this.head = {...this.head}
                    this.state.push(this.head)
                }
             }
            if(this.dir === Dir.UP) { 
                --(this.head.y)

                if(this.head.y < 0) {
                    this.state[this.state.length - 1]['split'] = true
                    this.head = {...this.head, y: this.boardSize.h - 1, split: false}
                    this.state.push(this.head)
                    this.head = {...this.head}
                    this.state.push(this.head)
                }
             }
            if(this.dir === Dir.DOWN) {
                 ++(this.head.y) 

                 if(this.head.y >= this.boardSize.h) {
                    this.state[this.state.length - 1]['split'] = true
                    this.head = {...this.head, y: 0, split: false}
                    this.state.push(this.head)
                    this.head = {...this.head}
                    this.state.push(this.head)
                }    
            }

           
            
          
            if((this.dir +  dir) % 2) {
                this.dir = dir

                this.head = {...this.head}
                this.state.push(this.head)
            }

            return true
        }

       
       

        if(this.growing > 0) {
            this.growing -= change
        } else {
            this.tailHide += change
        }
        this.headShow += change
        return false
    }

    // Logging
    log() {
        console.log(this.state, this.head, this.headShow, this.tail, this.tailHide)
        console.log("DIR = ", this.dir)
    }
}



