
import { Snake } from './model/Snake.js';

export default class Game {
    constructor(services, drawer, controller, sound) {
      this._services = services
      this._drawer = drawer
      this._controller = controller

      this._isRunning = false

      this._snake = new Snake({x: 2, y: 2, tileLen: 40, boardW: 40, boardH: 20})
     
      this._walls = []
      this._responsiveWalls = {
        horizontal: true,
        vertical: false
      }

      // SOUND
      this._sound = sound
      this._isSoundOn = false
      this._services.settingsService
        .getSettings()
        .then(s => {
          this._isSoundOn = s.isSoundActive})

      //SERVICES 
      this._scoresService = this._services.scoresService

      // FOOD
      this._food = []

      this._foodCntAtOnce = 3
      this._foodCntTotal = 100
      this._foodGenerated = 0

      // SCORE
      this._score = 0
      this._maxScore = 0
      this._bestScore = 0

      // EVENT SUBSCRIPTIONS
      this._scoreSubs = []
      this._winSubs = []
      this._gameOverSubs = []

      // LOOP TIME
      this._lastLoopCallTime = (new Date()).getTime()
      

      // Map
      this._map = null
    }

    // Transform map to game properties
    _readMap(map) {
      this._scoresService
        .getScore(map.id)
        .then(i => {
          this._bestScore = i ? i.score :0})

      this._map = map
      const content = map.content

      if('walls' in content) {
        const w = content.walls
        this._responsiveWalls.vertical = (w.vertical) ? true : false
        this._responsiveWalls.horizontal = (w.horizontal) ? true : false
      } else {
        this._responsiveWalls.vertical = false
        this._responsiveWalls.horizontal = false
      }

      if('foodCountTotal' in content) {
        const fct = content.foodCountTotal
        this._maxScore = fct
        this._foodCntTotal = fct
      }

      let fco = 1

      if('foodCountAtOnce' in content) {
        fco = content.foodCountAtOnce
        
        if('initialFoodPos' in content) {
          const ifp = content.initialFoodPos
          ifp.forEach((({x, y}) => {
            this._food.push({x, y})
            ++this._foodGenerated
          }))
        }
        else {
          for (let i = 0; i < fco; i++) {
            this._generateNewFood()
          }
        }
      }

      this._foodCntAtOnce = fco
    }

    restart() {
      this.start(this._map)
      this._scoreSubs.forEach(s => s(this._score))
    }


    _loop() {
      if(this._isRunning) {
        const currentTime = (new Date()).getTime();
        const delta = (currentTime - this._lastLoopCallTime) / 100;

        let d =  this._controller.dir 
        if(d === this._snake.dir) {
          d = this._controller.cachedDir || d
        }

        const speed = 30
        if(      this._snake.move(d, delta * speed) ){
          this._controller.canChangeDir = true
          this._controller.currentSnakeDir = this._snake.dir
          if(this._snake.dir === this._controller.cachedDir) {
            this._controller.dir = this._controller.cachedDir
            this._controller.cachedDir = null
          }

          if(this.wallCollisionCheck() || this.collisionCheck()) {
            this._gameOver()          
          }
        }

        this._drawer.clearDynamic()
        this._drawer.drawSnake(this._snake)

        const eaten = this.foodEatCheck()
        if(eaten > -1) {
          this._food.splice(eaten, 1)

        
          if(this._foodCntTotal > this._foodGenerated) {
            this._generateNewFood()
          }
          this._incScore()
          this._snake.eat(1)

          if(this._maxScore <= this._score) {
            this._win()
          }

          
          this._drawer.clearInteraction()
          this._drawer.drawFood(this._food)
        }

        this._lastLoopCallTime = currentTime;
        requestAnimationFrame(this._loop.bind(this))
      }
    }

    // Checks snake collision with itself
    collisionCheck() {
      const isBetween = (x, from, to) => {
        const [min, max] = from < to ? [from, to] : [to, from] 
        return x >= min && x <= max
      }
  
      const h = this._snake.head
      const sn = this._snake.state
  
      for(let i = 0; i < sn.length - 3; ++i) { //without head itself :)
        const s = sn[i]
        const e = sn[i + 1]
        
        if(s.split) {
          continue
        }
        if ((h.x === s.x && h.x === e.x && isBetween(h.y, s.y, e.y)) || 
            (h.y === s.y && h.y === e.y && isBetween(h.x, s.x, e.x)) ){
                 return true
               }
      }
  
      return false
    }

    // Checks snake collision with walls
    wallCollisionCheck() {
      if(this._responsiveWalls.vertical &&
          ([0, 39].includes(this._snake.head.x))) {
          return true
      }


      if(this._responsiveWalls.horizontal &&
        [0, 19].includes(this._snake.head.y) ) {
        return true
      }

      return false
    }

    // Checks snake collision with food
    foodEatCheck() {
      const h = this._snake.head
      const eatenFood = this._food.findIndex(f => f.x === h.x && f.y === h.y)
      
      return eatenFood
    }  

    _generateNewFood() {
      ++this._foodGenerated

      const offX = this._responsiveWalls.vertical ? 1 : 0
      const offY = this._responsiveWalls.horizontal ? 1 : 0

      let rndX = (Math.round(Math.random() * 100) % (40 - offX))
      let rndY = (Math.round(Math.random() * 100) % (20 - offY))
     
      rndX = (rndX === 0) ? offX : rndX
      rndY = (rndY === 0) ? offY : rndY

      this._food.push({x: rndX, y: rndY})
    }

    // render game to given 'container'
    renderTo(container) {
      this._drawer.renderTo(container)
    }

    _reset() {
      this._score = 0
      this._informScoreChange()

      this._snake = new Snake({x: 2, y: 2, minW: 10, maxW: 30, tileLen: 40, boardW: 40, boardH: 20})
      this._food = []
      this._foodGenerated = 0
      this._controller.reset()
      this._services.settingsService
        .getSettings()
        .then(s => {
          this._isSoundOn = s.isSoundActive})

    }

    // starts game configured for 'map'
    start(map) {
      this._reset()
      this._services
          .settingsService
          .getSettings()
          .then(s => {
            if(s.colorByWeather){
              this._services.weatherService
                  .getCurrentWeather()
                  .then(w => {
                    this._drawer.setColorsByWeather(w)
                    
                    this._drawer.clearInteraction()
                    this._drawer.drawFood(this._food)
                  })
            }
          })

      this._readMap(map)
      this._drawer.init()
      this._drawer.drawResponsiveWalls({...this._responsiveWalls})

      this._drawer.clearInteraction()
      this._drawer.drawFood(this._food)
      this._isRunning = true
      requestAnimationFrame(this._loop.bind(this))
    }


    _incScore() {
      ++this._score

      if(this._isSoundOn) {
        this._sound.eat()
      }

      this._informScoreChange()
    }

    _informScoreChange() {
      this._scoreSubs.forEach(s => s(this._score))
    }
    
    _saveScore() {
      if(this._bestScore < this._score) {
        this._scoresService.saveScore(
          this._map.id, 
          this._map.name, 
          this._score)
      }
    }

    _win() {
      this._isRunning = false
      this._saveScore()
      this._winSubs.forEach(s => s(this._score))
    }

    destroy() {
      this._isRunning = false
    }

    _gameOver() {
      this._isRunning = false

      this._saveScore()
      this._gameOverSubs.forEach(s => s())
    }

    registerScoreChangeEvent(cb) {
      this._scoreSubs.push(cb)
    }

    registerWinEvent(cb) {
      this._winSubs.push(cb)
    }

    registerGameOverEvent(cb) {
      this._gameOverSubs.push(cb)
    }
  }
  
  