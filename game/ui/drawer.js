import { Dir, getDir } from "../utils/directions.js"
import { useGradient, useWeatherColors } from "./colors.js"

class GameDrawer {
    constructor() {
        
    }

    renderTo(container) {}
}


export class CanvasGameDrawer extends GameDrawer {
    constructor() {
        super()

        const board = document.createElement('div')
        board.classList.add('game')

        const staticLayer = document.createElement('canvas')
        staticLayer.classList.add('board-canvas')
        staticLayer.width = 1600
        staticLayer.height = 800

        const interactionLayer = document.createElement('canvas')
        interactionLayer.classList.add('interaction-canvas')
        interactionLayer.width = 1600
        interactionLayer.height = 800

        const wallLayer = document.createElement('canvas')
        wallLayer.classList.add('wall-canvas')
        wallLayer.width = 1600
        wallLayer.height = 800

        const dynamicLayer = document.createElement('canvas')
        dynamicLayer.classList.add('game-canvas')
        dynamicLayer.width = 1600
        dynamicLayer.height = 800

        board.append(staticLayer, interactionLayer, wallLayer, dynamicLayer)

        this._squareSize = 40
        this._width = 40
        this._height = 20
        this._container = document.createElement('div')
        
        // LAYERS
        this._staticLayer = staticLayer
        this._dynamicLayer = dynamicLayer
        this._interactionLayer = interactionLayer
        this._wallLayer = interactionLayer
        this._board = board

        // CONTEXTS
        this._sCtx = staticLayer.getContext('2d')
        this._dCtx = dynamicLayer.getContext('2d')
        this._iCtx = interactionLayer.getContext('2d')
        this._wCtx = wallLayer.getContext('2d')

        // COLORS
        this._defaultSnakeColor = useGradient(this._dCtx);
        this._snakeColor = this._defaultSnakeColor

        this._defaultBoardColorLight = 'rgba(0, 255, 0, .2)'
        this._boardColorLight = this._defaultBoardColorLight

        this._defaultBoardColor = 'rgba(0, 255, 0, .3)'
        this._boardColor = this._defaultBoardColor

        this._defaultFoodColor = 'green'
        this._foodColor = this._defaultFoodColor
    }

    setColorsByWeather(w) {
        this.clear()

        const {primary, primaryLight, secondary, secondaryLight} = useWeatherColors(this.ctx, w)
        this.setSnakeColor(primary)
        this.setFoodColor(primaryLight)
        this._boardColor = secondary
        this._boardColorLight = secondaryLight

        this._renderStatic()
    }

    setSnakeColor(color) {
        this._snakeColor = color
    }

    setFoodColor(color) {
        this._foodColor = color
    }

    renderTo(container) {
        container.appendChild(this._board)
        this._container = container        
    }

    init() {
        this.clear()
        this.clearResponsiveWalls()
        this._snakeColor = this._defaultSnakeColor
        this._foodColor = this._defaultFoodColor
        this._boardColor = this._defaultBoardColor
        this._boardColorLight = this._defaultBoardColorLight
        this._renderStatic()
    }

    clear() {
        this._sCtx.clearRect(0, 0, this._staticLayer.width, this._staticLayer.height);
        this.clearDynamic()
    }

    clearResponsiveWalls() {
        this._wCtx.clearRect(0, 0, this._wallLayer.width, this._wallLayer.height);
    }
    clearDynamic() {
        this._dCtx.clearRect(0, 0, this._dynamicLayer.width, this._dynamicLayer.height);
    }

    clearInteraction() {
        this._iCtx.clearRect(0, 0, this._interactionLayer.width, this._interactionLayer.height);
    }


    _renderStatic() {
        const s = this._squareSize

        for(let i = 0; i < this._height; ++i) {
            for(let j = 0; j < this._width; ++j) {
                this._sCtx.fillStyle = ((i + j) % 2 == 0) ? this._boardColorLight : this._boardColor;
                const x = j * s;
                const y = i * s;
                this._sCtx.fillRect(x, y, s, s);
            }
        }
    }

    drawResponsiveWalls({vertical, horizontal}) {
        const s = this._squareSize


        if(vertical) {
            [0, this._width - 1].forEach(j => {
                for(let i = 0; i <  this._height; ++i) {
                    this._wCtx.fillStyle = 'black'
                    const x = j * s;
                    const y = i * s;
                    this._wCtx.fillRect(x, y, s, s);
                }
            })
        }

        if(horizontal) {
            [0, this._height - 1].forEach(i => {
                for(let j = 0; j <  this._width; ++j) {
                    this._wCtx.fillStyle = 'black'
                    const x = j * s;
                    const y = i * s;
                    this._wCtx.fillRect(x, y, s, s);
                }
            })
        }
        
    }

    drawSnake(snake) {
        this._dCtx.strokeStyle = this._snakeColor
        this._dCtx.lineCap = 'round';
  
        
        const MAX_SIZE = 30
        const MIN_SIZE = 22
        const SQUARE_SIZE = 40

        const fullSnakeLen = snake.len
        const growPerPart = (MAX_SIZE - MIN_SIZE) / fullSnakeLen
       

        let prevLw = MIN_SIZE

        let headX = 0
        let headY = 0
        for (let i = 0; i < snake.state.length - 1; ++i) {
            const state = snake.state
            const start = state[i]
            const end = state[i + 1]

            if(start.split) {
                continue
              }
            const startV = start.x + start.y;
            const endV = end.x + end.y;
            let parts = startV - endV
            parts = (parts < 0) ? -parts : parts

        
            const lw = prevLw + (growPerPart * parts)

            const of = (lw-prevLw) / 2
            this._dCtx.lineWidth = prevLw
  
            let dir = getDir(start, end)
         
            let startX = start.x * SQUARE_SIZE
            let startY = start.y * SQUARE_SIZE 
        
            let endX = {val: end.x * SQUARE_SIZE, of: 0}
            let endY = {val: end.y * SQUARE_SIZE, of : 0}
    
            startX += SQUARE_SIZE/2
            startY += (SQUARE_SIZE * .5) 
            endX.val += SQUARE_SIZE/2
            endY.val += (SQUARE_SIZE * .5) 

            if(dir === 1 || dir === 3) {
                endY.of = of
            } 
            else{
                endX.of = of
            } 

            
            if(i === snake.state.length - 2) {
                if(snake.dir === Dir.RIGHT ) {
                    endX.val += snake.headShow
                } 
                else if(snake.dir  === Dir.LEFT ) {
                    endX.val -= snake.headShow
                } 
                else if(snake.dir  === Dir.UP ) {
                    endY.val -= snake.headShow
                } 
                else if(snake.dir=== Dir.DOWN){
                    endY.val += snake.headShow
                }
                headX =  endX
                headY = endY
            }
            if(i === 0) {
                if(dir === Dir.RIGHT ) {
                    startX += snake.tailHide
                } 
                else if(dir  === Dir.LEFT ) {
                    startX -= snake.tailHide
                } 
                else if(dir  === Dir.UP ) {
                    startY -= snake.tailHide
                } 
                else if(dir  === Dir.DOWN ) {
                    startY += snake.tailHide
                }
            }
            this._dCtx.beginPath();
        
        
            this._dCtx.moveTo(startX, startY); 
            this._dCtx.lineTo(endX.val - endX.of, endY.val - endY.of);
            this._dCtx.stroke(); 
    
            
            this._dCtx.beginPath();

            this._dCtx.moveTo(startX, startY); 
            this._dCtx.lineTo(endX.val + endX.of, endY.val + endY.of);
            this._dCtx.stroke(); 

            prevLw = lw
        }

        this._drawSnakeHead(headX, headY, MAX_SIZE, snake)
    }

    get ctx() {
        return this._sCtx
    }

    _drawSnakeHead(headX, headY, MAX_SIZE, snake) {
        let offX = 0
        let offY = 0
        let changeX = 0
        let changeY = 0

        if(snake.dir === Dir.RIGHT ) {
            offX = -MAX_SIZE/2
            changeY = 10
        } 
        else if(snake.dir  === Dir.LEFT ) {
            offX = MAX_SIZE/2
            changeY = 10
        } 
        else if(snake.dir  === Dir.UP ) {
            offY = MAX_SIZE/2
            changeX = 10
        } 
        else if(snake.dir=== Dir.DOWN){
            offY = -MAX_SIZE/2
            changeX = 10
        }

        this._dCtx.beginPath();
        this._dCtx.arc(headX.val + offX + changeX, headY.val + offY + changeY, 7, 0, 2 * Math.PI, false);
        this._dCtx.fillStyle = 'white';
        this._dCtx.fill();
        this._dCtx.lineWidth = 5;
        this._dCtx.stroke();


        this._dCtx.beginPath();
        this._dCtx.arc(headX.val + offX - changeX, headY.val + offY - changeY, 7, 0, 2 * Math.PI, false);
        this._dCtx.fill();
        this._dCtx.lineWidth = 5;
        this._dCtx.stroke();
    }

    drawFood(foodArr) {
        foodArr.forEach(f => {
            this._iCtx.beginPath();
            this._iCtx.arc(f.x * 40 + 20, f.y * 40 + 20, 10, 0, 2 * Math.PI, false);
            this._iCtx.fillStyle = this._foodColor;
            this._iCtx.fill();
            this._iCtx.lineWidth = 5;
        })  
    }
}   