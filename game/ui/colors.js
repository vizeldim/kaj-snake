
export const useGradient = (ctx) => {
    const gradient = ctx.createLinearGradient(0, 0, 100, 1000);
    gradient.addColorStop("0", "rgb(0, 50, 0)");
    gradient.addColorStop("0.5" ,"rgb(0, 200, 0)");
    gradient.addColorStop("1.0" ,"rgb(255, 255, 0)");
    return gradient
}

// Returns color scheme by given temperature 'temp' and precipitation 'precip'
export const useWeatherColors = (ctx, {temp, precip}) => {
    const gr = useGradient(ctx)
    const pallete = {
        primary: gr,
        primaryLight: gr,
        secondary: 'rgba(0, 255, 0, .2)', 
        secondaryLight: 'rgba(0, 255, 0, .3)' 
    }

    if(precip > 0.2) {
        pallete.primary = 'blue'
        pallete.primaryLight = 'blue'
        pallete.secondary = 'rgba(0, 0, 255, .05)'
        pallete.secondaryLight = 'rgba(0, 0, 255, .1)'
    }

    else if(temp < 15) {
        pallete.primary = 'grey'
        pallete.primaryLight = 'grey'
        pallete.secondary = 'rgba(0, 0, 0, .05)'
        pallete.secondaryLight = 'rgba(0, 0, 255, .1)'
    } 

    else if(temp > 30)  {
        pallete.primary = 'orange'
        pallete.primaryLight = 'orange'
        pallete.secondary = 'rgba(255, 255, 0, .1)'
        pallete.secondaryLight = 'rgba(255, 255, 0, .3)'
    } 

    return pallete
}