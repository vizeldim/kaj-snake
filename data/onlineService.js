
class OnlineService {
    get isOnline() {
        return true
    }

    addOnlineEvent(cb) {}

    addOfflineEvent(cb) {}
}

export class NavigatorOnlineService extends OnlineService {
    get isOnline (){
        return navigator.onLine
    }

    addOnlineEvent(cb) {
        window.addEventListener('online', cb);
    }

    addOfflineEvent(cb) {
        window.addEventListener('offline', cb);
    }
}
