
class WeatherUpdateError extends Error {
    constructor(msg, cause) {
        super(msg)
        this.cause = cause
        this.name = 'Weather update error'
    }
}

class WeatherDataSource {
    constructor() {
        this._coords = {lat: 0, long: 0}

        this._weather = {
            temp: 0,
            precip: 0,
            location: ''
        }
    }

    set coords(newCoords) {
        this._coords = newCoords
    }

    get coords() {
        return this._coords
    }

    get needOnlineConn() {
        return false
    }

    async getCurrentWeather() {
        return Promise.resolve({...this._weather})
    }
}


export class ApiWeatherDataSource extends WeatherDataSource {
    constructor({key, url}, updateInterval) {
        super() 

        this._key = key
        this._url = url

        this._lastUpdate = 0
        this._updateInterval = updateInterval ?? (60 * 60 * 1000) // == 1 hour
    
        this._loaded = false
        this._loadedCoords = {...this._coords}
    }

    get _queryUrl () {
        const lat = this._coords.lat
        const long = this._coords.long

        return `${this._url}?key=${this._key}&q=${lat},${long}&aqi=no`
    }

    get needOnlineConn() {
        return true
    }

    async fetch() {
        const tempLoaded = {...this._loadedCoords }
        this._loadedCoords = {...this._coords}
        this._lastUpdate = Date.now()
        this._loaded = true

        return fetch(this._queryUrl)
            .then(resp => resp.json())
            .then(data => {

                const curData = data['current']

                const location = data['location']
                this._weather.temp = curData['temp_c']
                this._weather.precip = curData['precip_mm'] 
                this._weather.location = `${location['country']} - ${location['name']}`
                return {...this._weather}
            })
            .catch(e => {
                this._loadedCoords = tempLoaded
                this._loaded = false
                throw new WeatherUpdateError(e.message) })  
    }

    async getCurrentWeather(isCached) {
        // coords are different then loaded coords
        const corr = 0.000001
        if(!((this._loadedCoords.lat > (this._coords.lat - corr)) && 
            (this._loadedCoords.lat < (this._coords.lat + corr)))  
            ||
           !((this._loadedCoords.long > (this._coords.long - corr)) && 
            (this._loadedCoords.long < (this._coords.long + corr)))){ 
            return this.fetch()
        }

        else if(this._loaded && ((isCached === true) || ((Date.now() - this._lastUpdate) < this._updateInterval))) {
            return super.getCurrentWeather()
        }


        return this.fetch()
    }
}

