
export default class ServiceContainer {
    constructor(router,
                gameMapService,
                weatherService, 
                locationService, 
                settingsService, 
                scoresService,
                onlineService) {
        this._gameMapService = gameMapService
        this._weatherService = weatherService
        this._locationService = locationService
        this._settingsService = settingsService
        this._scoresService = scoresService
        this._onlineService = onlineService
        this._router = router
    }

    get weatherService() {
        return this._weatherService
    }

    get locationService() {
        return this._locationService
    }

    get settingsService() {
        return this._settingsService
    }

    get scoresService() {
        return this._scoresService
    }

    get onlineService() {
        return this._onlineService
    }
    
    get gameMapService() {
        return this._gameMapService
    }

    get router() {
        return this._router
    }
}