class SettingsDataSource {
    constructor(defaultSettings) {
        this._settings = {
            isSoundActive: false,
            colorByWeather: false
        }

        this._defaultSettings = defaultSettings ? 
            {...defaultSettings} : {...this._settings}
    }

    _validateSettings(objSettings) {
        for(const k of Object.keys(this._settings)) {
            if(!(k in objSettings)) {
                return false
            }
        }

        return true
    }

    async saveSettings(newSettings) {
        this._settings = {...newSettings}
    }

    async getSettings() {
        return Promise.resolve({...this._settings})
    }
}


export class LocalStorageSettingsDataSource extends SettingsDataSource {
    constructor(localStorageName, defaultSettings) {
        super(defaultSettings) 

        this._localStorageName = localStorageName
        this._loaded = false

    }


    _loadSettings() {
        let strSettings = localStorage.getItem(this._localStorageName)
        let objSettings = null

        if(strSettings) {
            try {
                const parsedSettings = JSON.parse(strSettings) 
                objSettings = parsedSettings
            } catch(e) {
                console.log("INVALID SETTINGS IN MEMORY => setting default")
                strSettings = false
            }
        }
        
        if(!strSettings || !this._validateSettings(objSettings)) {
            this.saveSettings(this._defaultSettings)
        }
        else {
            this._settings = {...objSettings} 
        }

        this._loaded = true
    }

    async saveSettings(newSettings) {
        super.saveSettings(newSettings)

        const strSettings = JSON.stringify(newSettings)
        localStorage.setItem(this._localStorageName, strSettings)
    }

    async getSettings() {
        if(!(this._loaded)) {
            this._loadSettings()
        }

        return Promise.resolve({...this._settings})
    } 
}
